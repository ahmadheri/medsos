@extends('adminlte.master')

@section('content')
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Profile {{ $profile->id }}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" method="POST" action="{{ route('profiles.update', [$profile->id] ) }}" enctype="multipart/form-data">
      @csrf
      @method('PATCH')
      <div class="card-body">
        <div class="form-group">
          <label for="nama_lengkap">Nama Lengkap</label>
          <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value="{{ $profile->nama_lengkap }}" placeholder="Isikan Nama Lengkap Anda">
          @error('nama_lengkap')
            <div class="alert alert-danger">{{ $message }}</div> 
          @enderror
        </div>
        <div class="form-group">
          <label for="alamat">Alamat</label>
          <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $profile->alamat }}" placeholder="Isikan Nama Lengkap Anda">
          @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div> 
          @enderror
        </div>
        <div class="form-group">
          <label for="foto">Foto</label> <br>
          @if ($profile->foto)
            <img src="{{ asset('storage/' . $profile->foto) }}" width="96px">
          @else 
            No Photo
          @endif
          <input type="file" class="form-control" id="foto" name="foto" >
          <small class="text-muted">Kosongkan jika tidak ingin mengubah foto</small>
        </div>

      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
@endsection