@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Profile User</h3>
    </div>

    @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
    @endif

    <div>
      <a href="{{ route('profiles.create') }}" class="btn btn-primary mt-2 ml-2">Create New Profile</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Nama Lengkap</th>
            <th>Alamat</th>
            <th>Foto</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($profiles as $key => $profile)
            <tr>
              <td>{{ $key + 1 }}</td>
              <td>{{ $profile->nama_lengkap }}</td>
              <td>{{ $profile->alamat }}</td>
              <td>
                @if($profile->foto)
                  <img src="{{ asset('storage/' . $profile->foto) }}" width="96px">
                @endif
              </td>
              <td style="display: flex">
                <a href="{{ route('profiles.edit', $profile->id) }}" class="btn btn-info btn-sm">edit</a>
                <form action="{{ route('profiles.destroy', $profile->id) }}" method="POST" onsubmit="return confirm('Yakin ingin menghapus data ini?')">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
          @empty
            <tr>
              <td colspan="4" align="center">No Record Found</td>
            </tr>
          @endforelse
            
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
    
  </div>
@endsection