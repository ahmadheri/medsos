@extends('adminlte.master')

@section('content')
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New Profile</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" method="POST" action="{{ route('profiles.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama_lengkap">Nama Lengkap</label>
          <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value="{{ old('nama_lengkap') }}" placeholder="Isikan Nama Lengkap Anda">
          @error('nama_lengkap')
            <div class="alert alert-danger">{{ $message }}</div> 
          @enderror
        </div>
        <div class="form-group">
          <label for="alamat">Alamat</label>
          <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat') }}" placeholder="Isikan Nama Lengkap Anda">
          @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div> 
          @enderror
        </div>
        <div class="form-group">
          <label for="foto">Foto</label>
          <input type="file" class="form-control" id="foto" name="foto" value="{{ old('foto') }}">
          @error('foto')
            <div class="alert alert-danger">{{ $message }}</div> 
          @enderror
        </div>

      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
@endsection