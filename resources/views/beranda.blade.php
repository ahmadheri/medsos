@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12 d-flex justify-content-center">

                <div class="card" style="width: 30rem;">
                    <div class="card-body">
                        <form action="{{ route('beranda.storePost') }}" method="POST" enctype="multipart/form-data"> 
                            @csrf
                            <h5 class="card-title">Create Post</h5>
                            <textarea class="form-control" name="isi_post" id="" cols="60" rows="4" placeholder="Tulisan postingan kamu disini"></textarea> <br>
                            <input class="" type="file" name="gambar"> <br><br>
                            <input type="submit" class="btn btn-primary" value="Bikin Postingan">

                        </form>
                        
                    </div>
                  </div>

            </div>
        </div>
    </div>

    <br>

    <div class="container">
        <div class="row">
            @foreach ($posts as $post)
            <div class="col-sm-12 d-flex justify-content-center mb-2">
                
                <div class="card" style="width: 26rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{ $post->user->name }}</h5>
                        <p class="card-text">{{ $post->isi }}</p>
                        @if ($post->gambar)
                            <img src="{{ asset('storage/' . $post->gambar) }}" class="card-img-top" alt="gambar_postingan" width="26rem"> <br>
                        @endif
                        <small class="text-muted">Dipost pada {{ $post->created_at }}</small>
                    </div>
                    <hr style="margin-top: -2px; margin-bottom: -2px;">
                    <div class="card-body">
                        <p class="card-text"></p>
                        <form action="" method="POST" style="display: flex;">
                            @csrf
                            <input type="text" class="form-control" name="komentar" placeholder="ketik komentar kamu disini">
                            <input type="submit" class="btn btn-primary btn-sm" value="Kirim">
                        </form>

                    </div>
                    
                </div>
                    
            </div>
            
            @endforeach
            
        </div>
    </div>
@endsection