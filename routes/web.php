<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/master', 'HomeController@index');


//ROUTE untuk halaman post (ibnu purnomo)
Route::get('/create', 'PostController@create');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/posts', 'PostController@index')->name('posts.index');

// Route Profiles User
Route::get('/profiles', 'ProfileController@index')->name('profiles.index');
Route::get('/profiles/create', 'ProfileController@create')->name('profiles.create');
Route::post('/profiles/store', 'ProfileController@store')->name('profiles.store');
Route::get('/profiles/{id}/edit', 'ProfileController@edit')->name('profiles.edit');
Route::patch('/profiles/{id}', 'ProfileController@update')->name('profiles.update');
Route::delete('/profiles/{id}', 'ProfileController@destroy')->name('profiles.destroy');

// Route Postingan
Route::get('/beranda', 'PostController@beranda')->name('beranda');
Route::post('/beranda/create', 'PostController@storePost')->name('beranda.storePost');


// Route Komentar
Route::post('/beranda/create', 'CommentController@storeComment')->name('beranda.storeComment');