<?php

namespace App\Http\Controllers;

use App\Post;
use Auth;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return view('posts.index');
    }

    public function beranda()
    {
        $posts = Post::all();

        return view('beranda', compact('posts'));
    }

    public function storePost(Request $request)
    {
        $post = new Post;
        $post->isi = $request->isi_post;
        $post->gambar = $request->gambar->store('pictures', 'public');
        $post->user_id = Auth::user()->id;
        $post->save();

        return redirect('beranda');

    }
}
