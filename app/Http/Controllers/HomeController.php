<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('adminlte.master');  //route nya mas heri dari view home -> saya ubah ke master
    }

    //fungsi adminlte (ibnu purnomo)
    public function master(){
        return view('adminlte.master');
    }

}
