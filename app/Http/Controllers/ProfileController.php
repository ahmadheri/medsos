<?php

namespace App\Http\Controllers;

use App\Profile;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $profiles = Profile::all();

        return view('profiles.index', compact('profiles'));
    }

    public function create()
    {
        return view('profiles.create');
    }

    public function store(Request $request)
    {      
        $request->validate([
            'nama_lengkap' => 'required',
            'alamat' => 'required',
            'foto' => 'required'
        ]);

        $profile = new Profile;
        $profile->nama_lengkap = $request->nama_lengkap;
        $profile->alamat = $request->alamat;
        if($request->foto){
            $profile->foto = $request->file('foto')->store('photos', 'public');
        }
        $profile->user_id = Auth::user()->id;
        $profile->save();

        return redirect()->route('profiles.index')->with('status', 'Profile berhasil dibuat');
    }

    public function edit($id)
    {
        $profile = Profile::findOrFail($id);

        return view('profiles.edit', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'nama_lengkap' => 'required',
            'alamat' => 'required',
        ]);
            
        $profile = Profile::findOrFail($id);

        $profile->nama_lengkap = $request->nama_lengkap;
        $profile->alamat = $request->alamat;

        if($request->foto){
            if($profile->foto && file_exists(storage_path('app/public/' . $profile->foto))){
                \Storage::delete('public/' . $profile->foto);
                $file = $request->file('foto')->store('photos', 'public');
                $profile->foto = $file;
            }
        }

        $profile->save();

        return redirect()->route('profiles.index', [$id] )->with('status', 'Profile user berhasil diupdate');
    }

    public function destroy($id)
    {
        $profile = Profile::findOrFail($id);

        $profile->delete();

        return redirect()->route('profiles.index')->with('status', 'Profile user berhasil dihapus');
    }
}
